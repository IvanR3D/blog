$(function() {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
     interval: 2000
    });

    $('#exampleModal').on('show.bs.modal', function(e) {
        console.log('Se muestra el model.');
    });
    $('#exampleModal').on('shown.bs.modal', function(e) {
        console.log('Se mostró el model.');
    });
});